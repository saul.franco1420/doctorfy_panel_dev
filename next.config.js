/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["images.unsplash.com"],
  },
  env: {
    API: process.env.API,
    API_KEY: process.env.API_KEY,
  },
};

module.exports = nextConfig;

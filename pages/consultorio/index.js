import Sidebar from "@/components/Sidebar";
import PrivateRoute from "@/components/PrivateRoute";

const Consultorio = () => {
  return (
    <>
      <PrivateRoute>
        <Sidebar />
        <main className="lg:pl-72  bg-gray-100">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-4">
              <div className="px-4 mb-10 sm:px-6 lg:px-8">
                {/* Agregar un nuevo componente TAILWIND */}
                {/* Agregar un nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* PANNEL DE ACCION CON SWITCH */}
                    <div className="bg-white mb-8 shadow-sm sm:rounded-lg border border-gray-200">
                      <div className="px-4 py-5 sm:p-6">
                        <h3
                          className="text-base font-semibold leading-6 text-gray-900"
                          id="renew-subscription-label"
                        >
                          Activación y bloqueo de calendario
                        </h3>
                        <div className="mt-2 sm:flex sm:items-start sm:justify-between">
                          <div className="max-w-xl text-sm text-gray-500">
                            <p id="renew-subscription-description">
                              Panel de activación y bloqueo de calendario para
                              este consultorio. Actualización en tiempo real.
                            </p>
                          </div>
                          <div className="mt-5 sm:ml-6 sm:mt-0 sm:flex sm:flex-shrink-0 sm:items-center">
                            {/* Enabled: "bg-indigo-600", Not Enabled: "bg-gray-200" */}
                            <button
                              type="button"
                              className="bg-gray-200 relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-indigo-600 focus:ring-offset-2"
                              role="switch"
                              aria-checked="false"
                              aria-labelledby="renew-subscription-label"
                              aria-describedby="renew-subscription-description"
                            >
                              {/* Enabled: "translate-x-5", Not Enabled: "translate-x-0" */}
                              <span
                                aria-hidden="true"
                                className="translate-x-0 inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out"
                              />
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* INPUT TEXTO (Nombre del consultorio) */}
                    <div className="flex flex-col mb-8 sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Nombre del consultorio
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Nombre del consultorio"
                        />
                      </div>
                      {/* INPUT TEXTO (Código Postal) */}
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Código Postal
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder={"00000"}
                        />
                      </div>
                    </div>
                    {/* INPUT TEXTO (Calle) */}
                    <div className="flex flex-col mb-8 sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Calle
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Calle"
                        />
                      </div>
                      {/* INPUT TEXTO (Sitio Web) */}
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Sitio Web
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Ejemplo.com"
                        />
                      </div>
                    </div>
                    {/* INPUT TEXTO (Ciudad) */}
                    <div className="flex flex-col sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Ciudad
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Ciudad/Estado"
                        />
                      </div>
                      {/* INPUT TEXTO (Teléfono) */}
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Teléfono
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="123-4567-8910"
                        />
                      </div>
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Agregar un nuevo componente TAILWIND */}
                {/* Agregar un nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    <h3 className="ml-2 mb-4 text-base font-semibold leading-6 text-gray-900">
                      ¿Cual es el precio de tus servicios en este consultorio?
                    </h3>
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* INPUT TEXTO (PRIMERA VISITA) */}
                    <div className="flex flex-col sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                      <div className="relative rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Primera visita
                        </label>
                        <div className="mt-1 relative rounded-md shadow-sm">
                          <div className="absolute inset-y-0 left-0 flex items-center pointer-events-none">
                            <span className="text-gray-500 pl-0 mr-0 sm:text-sm">
                              $
                            </span>
                          </div>
                          <input
                            type="text"
                            name="name"
                            id="name"
                            className="form-input block w-full pl-3 border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder={0.0}
                          />
                        </div>
                      </div>
                      {/* INPUT TEXTO (VISITA SUBSECUENTE) */}
                      <div className="relative rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Visita subsecuente
                        </label>
                        <div className="mt-1 relative rounded-md shadow-sm">
                          <div className="absolute inset-y-0 left-0 flex items-center pointer-events-none">
                            <span className="text-gray-500 pl-0 mr-0 sm:text-sm">
                              $
                            </span>
                          </div>
                          <input
                            type="text"
                            name="name"
                            id="name"
                            className="form-input block w-full pl-3 border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder={0.0}
                          />
                        </div>
                      </div>
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Agregar un nuevo componente TAILWIND */}
                {/* Agregar un nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* INFO CARDS (METODOS DE PAGO) */}
                    <fieldset className="w-full mx-auto">
                      <legend className="ml-2 text-base font-semibold leading-6 text-gray-900">
                        Metodos de pago aceptados
                      </legend>
                      <div className="mt-4 grid grid-cols-1 sm:grid-cols-2 gap-y-6 sm:gap-x-4">
                        <label className="mr-1 mb-2 border flex cursor-pointer rounded-lg hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Existing Customers"
                            className="sr-only"
                            aria-labelledby="project-type-1-label"
                            aria-describedby="project-type-1-description-0 project-type-1-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-1-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Efectivo
                              </span>
                              <span
                                id="project-type-1-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si aceptas efectivo.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                        <label className="ml-1 mb-2 flex border rounded-lg cursor-pointer hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Trial Users"
                            className="sr-only"
                            aria-labelledby="project-type-2-label"
                            aria-describedby="project-type-2-description-0 project-type-2-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-2-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Tarjeta de crédito
                              </span>
                              <span
                                id="project-type-2-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si aceptas tarjeta de
                                crédito.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                        <label className="mr-1 border flex cursor-pointer rounded-lg hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Existing Customers"
                            className="sr-only"
                            aria-labelledby="project-type-1-label"
                            aria-describedby="project-type-1-description-0 project-type-1-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-1-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Tarjeta de débito
                              </span>
                              <span
                                id="project-type-1-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si aceptas tarjeta de
                                débito.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                        <label className="ml-1 flex border rounded-lg cursor-pointer hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Trial Users"
                            className="sr-only"
                            aria-labelledby="project-type-2-label"
                            aria-describedby="project-type-2-description-0 project-type-2-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-2-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Transferencia
                              </span>
                              <span
                                id="project-type-2-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si aceptas pago por
                                transferencia.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                      </div>
                    </fieldset>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Agregar un nuevo componente TAILWIND */}
                {/* Agregar un nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* INFO CARDS (CALENDARIO-SEGMENTACION DE TIEMPO) */}
                    <fieldset className="w-full mx-auto">
                      <legend className="ml-2 text-base font-semibold leading-6 text-gray-900">
                        Horario de cita
                      </legend>
                      <div className="mt-4 grid grid-cols-1 sm:grid-cols-2 gap-y-6 sm:gap-x-4">
                        <label className="mr-1 mb-2 border flex cursor-pointer rounded-lg hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Existing Customers"
                            className="sr-only"
                            aria-labelledby="project-type-1-label"
                            aria-describedby="project-type-1-description-0 project-type-1-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-1-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Citas con duración de: 1 hora con 30 minutos
                              </span>
                              <span
                                id="project-type-1-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si coincide con la
                                duración de tus citas.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                        <label className="ml-1 mb-2 flex border rounded-lg cursor-pointer hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Trial Users"
                            className="sr-only"
                            aria-labelledby="project-type-2-label"
                            aria-describedby="project-type-2-description-0 project-type-2-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-2-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Citas con duración de: 1 hora
                              </span>
                              <span
                                id="project-type-2-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si coincide con la
                                duración de tus citas.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                        <label className="mr-1 border flex cursor-pointer rounded-lg hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Existing Customers"
                            className="sr-only"
                            aria-labelledby="project-type-1-label"
                            aria-describedby="project-type-1-description-0 project-type-1-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-1-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Citas con duración de: 45 minutos
                              </span>
                              <span
                                id="project-type-1-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si coincide con la
                                duración de tus citas.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                        <label className="ml-1 flex border rounded-lg cursor-pointer hover:bg-gray-50 bg-white p-4 shadow-sm focus:outline-none">
                          <input
                            type="radio"
                            name="project-type"
                            defaultValue="Trial Users"
                            className="sr-only"
                            aria-labelledby="project-type-2-label"
                            aria-describedby="project-type-2-description-0 project-type-2-description-1"
                          />
                          <span className="flex flex-1">
                            <span className="flex flex-col">
                              <span
                                id="project-type-2-label"
                                className="block text-sm font-medium text-gray-900"
                              >
                                Citas con duración de: 30 minutos
                              </span>
                              <span
                                id="project-type-2-description-0"
                                className="mt-1 flex items-center text-sm text-gray-500"
                              >
                                Selecciona este campo si coincide con la
                                duración de tus citas.
                              </span>
                            </span>
                          </span>
                          <svg
                            className="h-5 w-5 text-custom-color"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </label>
                      </div>
                    </fieldset>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Agregar un nuevo componente TAILWIND */}
                {/* Agregar un nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* Agregar un nuevo componente TAILWIND */}
                    {/* INPUT (BUSCADOR DE SEGUROS MEDICOS) */}
                    <h3 className="ml-2 mb-4 text-base font-semibold leading-6 text-gray-900">
                      Aseguradoras aceptadas en esta dirección
                    </h3>
                    <div>
                      <div className="relative mt-2">
                        <input
                          id="combobox"
                          type="text"
                          className="w-full rounded-md border-0 bg-white py-1.5 pl-3 pr-12 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-custom-color sm:text-sm sm:leading-6"
                          role="combobox"
                          aria-controls="options"
                          aria-expanded="false"
                        />
                        <button
                          type="button"
                          className="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none"
                        >
                          <svg
                            className="h-5 w-5 text-gray-400"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 3a.75.75 0 01.55.24l3.25 3.5a.75.75 0 11-1.1 1.02L10 4.852 7.3 7.76a.75.75 0 01-1.1-1.02l3.25-3.5A.75.75 0 0110 3zm-3.76 9.2a.75.75 0 011.06.04l2.7 2.908 2.7-2.908a.75.75 0 111.1 1.02l-3.25 3.5a.75.75 0 01-1.1 0l-3.25-3.5a.75.75 0 01.04-1.06z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </button>
                        <ul
                          className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm"
                          id="options"
                          role="listbox"
                        >
                          {/*
          Combobox option, manage highlight styles based on mouseenter/mouseleave and keyboard navigation.
  
          Active: "text-white bg-indigo-600", Not Active: "text-gray-900"
        */}
                          <li
                            className="relative cursor-default select-none py-2 pl-3 pr-9 text-gray-900"
                            id="option-0"
                            role="option"
                            tabIndex={-1}
                          >
                            {/* Selected: "font-semibold" */}
                            <span className="block truncate">
                              Leslie Alexander
                            </span>
                            {/*
            Checkmark, only display for selected option.
  
            Active: "text-white", Not Active: "text-indigo-600"
          */}
                            <span className="absolute inset-y-0 right-0 flex items-center pr-4 text-custom-color">
                              <svg
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                                aria-hidden="true"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            </span>
                          </li>
                          {/* More items... */}
                        </ul>
                      </div>
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </PrivateRoute>
    </>
  );
};

export default Consultorio;

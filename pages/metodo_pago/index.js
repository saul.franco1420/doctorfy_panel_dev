import Sidebar from "@/components/Sidebar";
import Link from "next/link";
import Image from "next/image";
import PrivateRoute from "@/components/PrivateRoute";

const MetodoPago = () => {
  return (
    <>
      <PrivateRoute>
        <Sidebar />
        <main className="lg:pl-72  bg-gray-100">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-4">
              <div className="px-4 sm:px-6 lg:px-8">
                <div className="lg:grid lg:grid-cols-12 lg:gap-x-5">
                  <aside className="px-2 py-6 sm:px-6 lg:col-span-3 lg:px-0 lg:py-0">
                    <nav className="space-y-1">
                      <Link
                        href="#"
                        className="text-gray-700 hover:bg-white hover:text-custom-color group flex items-center rounded-md px-3 py-2 text-sm font-medium"
                        aria-current="page"
                      >
                        <svg
                          className="text-gray-400 group-hover:text-custom-color -ml-1 mr-3 h-6 w-6 flex-shrink-0"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                          />
                        </svg>
                        <span className="truncate">Cuenta</span>
                      </Link>
                      <Link
                        href="#"
                        className="text-custom-color bg-gray-50 hover:text-custom-color group flex items-center rounded-md px-3 py-2 text-sm font-medium"
                      >
                        <svg
                          className="text-custom-color group-hover:text-custom-color -ml-1 mr-3 h-6 w-6 flex-shrink-0"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z"
                          />
                        </svg>
                        <span className="truncate">Metodo de pago</span>
                      </Link>
                    </nav>
                  </aside>
                  <div className="space-y-6 sm:px-6 lg:col-span-9 lg:px-0">
                    <form action="#" method="POST">
                      <div className="sm:overflow-hidden sm:rounded-md">
                        {/* Nuevo componente TAILWIND aqui */}
                        {/* Nuevo componente TAILWIND aqui */}
                        {/* PAYMENT METHOD */}
                        <div className="bg-white mb-8 shadow sm:rounded-lg">
                          <div className="px-4 py-5 sm:p-6">
                            <h3 className="text-base font-semibold leading-6 text-gray-900">
                              Payment method
                            </h3>
                            <div className="mt-5">
                              <div className="rounded-md bg-gray-50 px-6 py-5 sm:flex sm:items-start sm:justify-between">
                                <h4 className="sr-only">Visa</h4>
                                <div className="sm:flex sm:items-start">
                                  <svg
                                    className="h-8 w-auto sm:h-6 sm:flex-shrink-0"
                                    viewBox="0 0 36 24"
                                    aria-hidden="true"
                                  >
                                    <rect
                                      width={36}
                                      height={24}
                                      fill="#224DBA"
                                      rx={4}
                                    />
                                    <path
                                      fill="#fff"
                                      d="M10.925 15.673H8.874l-1.538-6c-.073-.276-.228-.52-.456-.635A6.575 6.575 0 005 8.403v-.231h3.304c.456 0 .798.347.855.75l.798 4.328 2.05-5.078h1.994l-3.076 7.5zm4.216 0h-1.937L14.8 8.172h1.937l-1.595 7.5zm4.101-5.422c.057-.404.399-.635.798-.635a3.54 3.54 0 011.88.346l.342-1.615A4.808 4.808 0 0020.496 8c-1.88 0-3.248 1.039-3.248 2.481 0 1.097.969 1.673 1.653 2.02.74.346 1.025.577.968.923 0 .519-.57.75-1.139.75a4.795 4.795 0 01-1.994-.462l-.342 1.616a5.48 5.48 0 002.108.404c2.108.057 3.418-.981 3.418-2.539 0-1.962-2.678-2.077-2.678-2.942zm9.457 5.422L27.16 8.172h-1.652a.858.858 0 00-.798.577l-2.848 6.924h1.994l.398-1.096h2.45l.228 1.096h1.766zm-2.905-5.482l.57 2.827h-1.596l1.026-2.827z"
                                    />
                                  </svg>
                                  <div className="mt-3 sm:ml-4 sm:mt-0">
                                    <div className="text-sm font-medium text-gray-900">
                                      Ending with 4242
                                    </div>
                                    <div className="mt-1 text-sm text-gray-600 sm:flex sm:items-center">
                                      <div>Expires 12/20</div>
                                      <span
                                        className="hidden sm:mx-2 sm:inline"
                                        aria-hidden="true"
                                      >
                                        ·
                                      </span>
                                      <div className="mt-1 sm:mt-0">
                                        Last updated on 22 Aug 2017
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="mt-4 sm:ml-6 sm:mt-0 sm:flex-shrink-0">
                                  <button
                                    type="button"
                                    className="inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                                  >
                                    Edit
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="overflow-hidden rounded-lg bg-white shadow">
                          <div className="px-4 py-5 sm:p-6">
                            <div>
                              <fieldset>
                                <div className="flex items-center justify-between">
                                  <legend className="mb-2 block text-sm font-medium leading-6 text-gray-900">
                                    Card Details
                                  </legend>
                                  <Image
                                    width={50}
                                    height={50}
                                    src="images/mastercard.svg"
                                    className="mb-3 h-5"
                                    alt="image"
                                  />
                                  <Image
                                    width={50}
                                    height={50}
                                    src="images/visa.svg"
                                    className="mb-3 h-4"
                                    alt="image"
                                  />
                                  <Image
                                    width={50}
                                    height={50}
                                    src="images/amex-minified.svg"
                                    className="mb-3 h-4"
                                    alt="image"
                                  />
                                  <Image
                                    width={50}
                                    height={50}
                                    src="images/discover.svg"
                                    className="mb-3 h-4"
                                    alt="image"
                                  />
                                </div>
                                <div className="mt-2 -space-y-px rounded-md bg-white shadow-sm">
                                  <div>
                                    <label
                                      htmlFor="card-number"
                                      className="sr-only"
                                    >
                                      Card number
                                    </label>
                                    <input
                                      type="text"
                                      name="card-number"
                                      id="card-number"
                                      className="pl-3 relative block w-full rounded-none rounded-t-md border-0 bg-transparent py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                      placeholder="Card number"
                                    />
                                  </div>
                                  <div className="flex -space-x-px">
                                    <div className="w-1/2 min-w-0 flex-1">
                                      <label
                                        htmlFor="card-expiration-date"
                                        className="sr-only"
                                      >
                                        Expiration date
                                      </label>
                                      <input
                                        type="text"
                                        name="card-expiration-date"
                                        id="card-expiration-date"
                                        className="pl-3 relative block w-full rounded-none rounded-bl-md border-0 bg-transparent py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        placeholder="MM / YY"
                                      />
                                    </div>
                                    <div className="min-w-0 flex-1">
                                      <label
                                        htmlFor="card-cvc"
                                        className="sr-only"
                                      >
                                        CVC
                                      </label>
                                      <input
                                        type="text"
                                        name="card-cvc"
                                        id="card-cvc"
                                        className="pl-3 relative block w-full rounded-none rounded-br-md border-0 bg-transparent py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        placeholder="CVC"
                                      />
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                              <fieldset className="mt-6 bg-white">
                                <legend className="block text-sm font-medium leading-6 text-gray-900">
                                  Billing address
                                </legend>
                                <div className="mt-2 -space-y-px rounded-md shadow-sm">
                                  <div>
                                    <label
                                      htmlFor="country"
                                      className="sr-only"
                                    >
                                      Country
                                    </label>
                                    <select
                                      id="country"
                                      name="country"
                                      autoComplete="country-name"
                                      className="relative block w-full rounded-none rounded-t-md border-0 bg-transparent py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                    >
                                      <option>United States</option>
                                      <option>Canada</option>
                                      <option>Mexico</option>
                                    </select>
                                  </div>
                                  <div>
                                    <label
                                      htmlFor="postal-code"
                                      className="sr-only"
                                    >
                                      ZIP / Postal code
                                    </label>
                                    <input
                                      type="text"
                                      name="postal-code"
                                      id="postal-code"
                                      autoComplete="postal-code"
                                      className="pl-3 relative block w-full rounded-none rounded-b-md border-0 bg-transparent py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                      placeholder="ZIP / Postal code"
                                    />
                                  </div>
                                </div>
                              </fieldset>
                              <div className="flex mt-4">
                                <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                                  Agregar metodo de pago
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </PrivateRoute>
    </>
  );
};

export default MetodoPago;

import React, { useEffect } from 'react';
import { useRouter } from 'next/router';


/**
 * A placeholder component for redirection.
 * This component automatically redirects to the "/login" page when rendered.
 * @returns {JSX.Element} - The rendered JSX element.
*/

const Index = () => {
  const router = useRouter();

  useEffect(() => {
    // Automatically redirect to the "/login" page
    router.push("/login");
  }, []);

  return <></>; // This component doesn't render anything visible
};

export default Index;

import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from 'next/router';
import styles from "@/styles/styles_pages/login.module.css";
import Image from "next/image";
import Button from "@/components/Buttons";
import InputText from "@/components/Inputs/Text";
import { accessLogin } from "@/redux/actions/login";
import { StatusLogin } from "@/redux/selector/login";

const Login = () => {

  const dispatch = useDispatch();
  const router = useRouter();

  const { loadingLogin, errorLogin, login } = StatusLogin();

  useEffect(() => {
    // Check if there's a token in the 'login' state or in sessionStorage
    const token = login || sessionStorage.getItem("login");
    if (token) {
      // If a token is found, redirect to the dashboard or another desired route
      router.push("/dashboard");
    }
  }, [login]);

  const [formData, setFormData] = useState({
    user_login: "",
    password_login: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleClick = () => {
    dispatch(accessLogin(formData));
    setFormData({
      user_login: "",
      password_login: "",
    });
  };

  return (
    <section className={styles.login}>
      <div className={styles.section_login}>
        <div className={styles.first_login}>
          <div className={styles.form_login}>
            <p className={styles.text_title_login}>Iniciar Sesión</p>
            <InputText
              key={"email"}
              field={"email"}
              formData={formData}
              handleChange={handleChange}
              label={"Correo Electrónico"}
              styleInput={"input_form"}
              name={"user_login"}
              type={"text"}
              placeholder={"Escribe tu correo eléctronico"}
            />
            <InputText
              key={"password_login"}
              field={"password_login"}
              formData={formData}
              handleChange={handleChange}
              label={"Contraseña"}
              styleInput={"input_form"}
              name={"password_login"}
              type={"password"}
              placeholder={"Escribe tu contraseña"}
            />{" "}
            {errorLogin && (
              <p className={styles.text_error_login}>
                Su usuario y/o contraseña son incorrectos.
              </p>
            )}
          </div>
          <div className={styles.button_login}>
            <Button
              text={"Iniciar sesión"}
              onClick={handleClick}
              typeCss="button_register"
              loading={loadingLogin}
            />
            <p className={styles.text_subtitle_login}>
              ¿No tienes cuenta? <strong>Registrarme</strong>
            </p>
          </div>
        </div>
        <div className={styles.second_login}>
          <div className={styles.first_second_login}>
            <h1 className={styles.title_login}>
              Tu bienestar, la clave de la felicidad ¡Inicia sesión ahora!
            </h1>
          </div>
          <div className={styles.second_second_login}></div>
          <div className={styles.third_second_login}>
            <Image
              src="/images/logo.svg"
              alt="Logo of doctorfy"
              width={200}
              height={100}
              priority
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Login;

import Sidebar from "@/components/Sidebar";
import Link from "next/link";
import Image from "next/image";
import PrivateRoute from "@/components/PrivateRoute";

const Estadistica = () => {
  return (
    <PrivateRoute>
      <div className="h-full">
        <Sidebar />
        <main className="lg:pl-72  bg-gray-100">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-4">
              <div className="px-4 sm:px-6 lg:px-8">
                <div className="pb-8">
                  <h3 className="text-base font-semibold leading-6 text-gray-900">
                    Últimos 30 dias
                  </h3>
                  <dl className="mt-5 grid grid-cols-1 divide-y divide-gray-200 overflow-hidden rounded-lg bg-white shadow md:grid-cols-3 md:divide-x md:divide-y-0">
                    <div className="px-4 py-5 sm:p-6">
                      <dt className="text-base font-normal text-gray-900">
                        Consultas concluidas
                      </dt>
                      <dd className="mt-1 flex items-baseline justify-between md:block lg:flex">
                        <div className="flex items-baseline text-2xl font-semibold text-gray-700">
                          91
                          <span className="ml-2 text-sm font-medium text-gray-500">
                            mes pasado 78
                          </span>
                        </div>
                        <div className="inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-green-100 text-green-800 md:mt-2 lg:mt-0">
                          <svg
                            className="-ml-1 mr-0.5 h-5 w-5 flex-shrink-0 self-center text-green-500"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 17a.75.75 0 01-.75-.75V5.612L5.29 9.77a.75.75 0 01-1.08-1.04l5.25-5.5a.75.75 0 011.08 0l5.25 5.5a.75.75 0 11-1.08 1.04l-3.96-4.158V16.25A.75.75 0 0110 17z"
                              clipRule="evenodd"
                            />
                          </svg>
                          <span className="sr-only"> Increased by </span>
                          12%
                        </div>
                      </dd>
                    </div>
                    <div className="px-4 py-5 sm:p-6">
                      <dt className="text-base font-normal text-gray-900">
                        Pacientes
                      </dt>
                      <dd className="mt-1 flex items-baseline justify-between md:block lg:flex">
                        <div className="flex items-baseline text-2xl font-semibold text-gray-700">
                          58
                          <span className="ml-2 text-sm font-medium text-gray-500">
                            mes pasado 40
                          </span>
                        </div>
                        <div className="inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-green-100 text-green-800 md:mt-2 lg:mt-0">
                          <svg
                            className="-ml-1 mr-0.5 h-5 w-5 flex-shrink-0 self-center text-green-500"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 17a.75.75 0 01-.75-.75V5.612L5.29 9.77a.75.75 0 01-1.08-1.04l5.25-5.5a.75.75 0 011.08 0l5.25 5.5a.75.75 0 11-1.08 1.04l-3.96-4.158V16.25A.75.75 0 0110 17z"
                              clipRule="evenodd"
                            />
                          </svg>
                          <span className="sr-only"> Increased by </span>
                          2.02%
                        </div>
                      </dd>
                    </div>
                    <div className="px-4 py-5 sm:p-6">
                      <dt className="text-base font-normal text-gray-900">
                        Visitas a tu perfil
                      </dt>
                      <dd className="mt-1 flex items-baseline justify-between md:block lg:flex">
                        <div className="flex items-baseline text-2xl font-semibold text-gray-700">
                          24,038
                          <span className="ml-2 text-sm font-medium text-gray-500">
                            mes pasado 19,390
                          </span>
                        </div>
                        <div className="inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-red-100 text-red-800 md:mt-2 lg:mt-0">
                          <svg
                            className="-ml-1 mr-0.5 h-5 w-5 flex-shrink-0 self-center text-red-500"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 3a.75.75 0 01.75.75v10.638l3.96-4.158a.75.75 0 111.08 1.04l-5.25 5.5a.75.75 0 01-1.08 0l-5.25-5.5a.75.75 0 111.08-1.04l3.96 4.158V3.75A.75.75 0 0110 3z"
                              clipRule="evenodd"
                            />
                          </svg>
                          <span className="sr-only"> Decreased by </span>
                          4.05%
                        </div>
                      </dd>
                    </div>
                  </dl>
                </div>
                <ul
                  role="list"
                  className="pb-8 grid grid-cols-1 gap-x-6 gap-y-8 lg:grid-cols-3 xl:gap-x-8"
                >
                  <li className="overflow-hidden rounded-xl border border-gray-200">
                    <div className="flex items-center gap-x-4 border-b border-gray-900/5 bg-white p-6">
                      <Image
                        width={50}
                        height={50}
                        src="images/cartera.svg"
                        alt="Tuple"
                        className="h-12 w-12 p-3 flex-none rounded-lg bg-white object-cover ring-1 ring-gray-900/10"
                      />
                      <div className="text-sm font-medium leading-6 text-gray-900">
                        Ingresos
                      </div>
                      <div className="relative ml-auto">
                        <button
                          type="button"
                          className="-m-2.5 block p-2.5 text-gray-400 hover:text-gray-500"
                          id="options-menu-0-button"
                          aria-expanded="false"
                          aria-haspopup="true"
                        >
                          <span className="sr-only">Open options</span>
                          <svg
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path d="M3 10a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0zM8.5 10a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0zM15.5 8.5a1.5 1.5 0 100 3 1.5 1.5 0 000-3z" />
                          </svg>
                        </button>
                        {/*
            Dropdown menu, show/hide based on menu state.
  
            Entering: "transition ease-out duration-100"
              From: "transform opacity-0 scale-95"
              To: "transform opacity-100 scale-100"
            Leaving: "transition ease-in duration-75"
              From: "transform opacity-100 scale-100"
              To: "transform opacity-0 scale-95"
          */}
                        <div
                          className="absolute right-0 z-10 mt-0.5 w-32 origin-top-right rounded-md bg-white py-2 shadow-lg ring-1 ring-gray-900/5 focus:outline-none"
                          role="menu"
                          aria-orientation="vertical"
                          aria-labelledby="options-menu-0-button"
                          tabIndex={-1}
                        >
                          {/* Active: "bg-gray-50", Not Active: "" */}
                          <Link
                            href="/inicio"
                            className="block px-3 py-1 text-sm leading-6 text-gray-900"
                            role="menuitem"
                            tabIndex={-1}
                            id="options-menu-0-item-0"
                          >
                            Últimos 30 dias
                            <span className="sr-only">, Tuple</span>
                          </Link>
                          <Link
                            href="#"
                            className="block px-3 py-1 text-sm leading-6 text-gray-900"
                            role="menuitem"
                            tabIndex={-1}
                            id="options-menu-0-item-1"
                          >
                            Año<span className="sr-only">, Tuple</span>
                          </Link>
                        </div>
                      </div>
                    </div>
                    <dl className="bg-white -my-3 divide-y divide-gray-100 px-6 py-4 text-sm leading-6">
                      <div className="flex justify-between gap-x-4 py-3">
                        <dt className="text-gray-500">
                          Ingresos registrados en 2023
                        </dt>
                        <dd className="text-gray-700">
                          <time dateTime="2022-12-13">December 13, 2022</time>
                        </dd>
                      </div>
                      <div className="flex justify-between gap-x-4 py-3">
                        <dt className="text-gray-500">Total</dt>
                        <dd className="flex items-start gap-x-2">
                          <div className="font-medium text-gray-900">
                            $62,000.00
                          </div>
                        </dd>
                      </div>
                    </dl>
                  </li>
                  <li className="overflow-hidden rounded-xl border border-gray-200">
                    <div className="flex items-center gap-x-4 border-b border-gray-900/5 bg-white p-6">
                      <Image
                        width={50}
                        height={50}
                        src="images/estrella.svg"
                        alt="SavvyCal"
                        className="h-12 w-12 p-3 flex-none rounded-lg bg-white object-cover ring-1 ring-gray-900/10"
                      />
                      <div className="text-sm font-medium leading-6 text-gray-900">
                        Calificación Promedio
                      </div>
                      <div className="relative ml-auto">
                        <button
                          type="button"
                          className="-m-2.5 block p-2.5 text-gray-400 hover:text-gray-500"
                          id="options-menu-1-button"
                          aria-expanded="false"
                          aria-haspopup="true"
                        >
                          <span className="sr-only">Open options</span>
                          <svg
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path d="M3 10a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0zM8.5 10a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0zM15.5 8.5a1.5 1.5 0 100 3 1.5 1.5 0 000-3z" />
                          </svg>
                        </button>
                        {/*
            Dropdown menu, show/hide based on menu state.
  
            Entering: "transition ease-out duration-100"
              From: "transform opacity-0 scale-95"
              To: "transform opacity-100 scale-100"
            Leaving: "transition ease-in duration-75"
              From: "transform opacity-100 scale-100"
              To: "transform opacity-0 scale-95"
          */}
                        <div
                          className="absolute right-0 z-10 mt-0.5 w-32 origin-top-right rounded-md bg-white py-2 shadow-lg ring-1 ring-gray-900/5 focus:outline-none"
                          role="menu"
                          aria-orientation="vertical"
                          aria-labelledby="options-menu-1-button"
                          tabIndex={-1}
                        >
                          {/* Active: "bg-gray-50", Not Active: "" */}
                          <Link
                            href="/inicio"
                            className="block px-3 py-1 text-sm leading-6 text-gray-900"
                            role="menuitem"
                            tabIndex={-1}
                            id="options-menu-1-item-0"
                          >
                            Últimos 30 dias
                            <span className="sr-only">, SavvyCal</span>
                          </Link>
                          <Link
                            href="/inicio"
                            className="block px-3 py-1 text-sm leading-6 text-gray-900"
                            role="menuitem"
                            tabIndex={-1}
                            id="options-menu-1-item-1"
                          >
                            Año<span className="sr-only">, SavvyCal</span>
                          </Link>
                        </div>
                      </div>
                    </div>
                    <dl className="-my-3 bg-white divide-y divide-gray-100 px-6 py-4 text-sm leading-6">
                      <div className="flex justify-between gap-x-4 py-3">
                        <dt className="text-gray-500">
                          Calidad general promedio
                        </dt>
                        <dd className="text-gray-700">
                          <time dateTime="2023-01-22">January 22, 2023</time>
                        </dd>
                      </div>
                      <div className="flex justify-between gap-x-4 py-3">
                        <dt className="text-gray-500">Calificación</dt>
                        <dd className="flex items-start gap-x-2">
                          <Image
                            width={50}
                            height={50}
                            src="images/estrella.svg"
                            className="h-5 pt-1"
                            alt="image"
                          />
                          <div className="font-medium text-gray-900">4.8</div>
                        </dd>
                      </div>
                    </dl>
                  </li>
                  <li className="overflow-hidden rounded-xl border border-gray-200">
                    <div className="flex items-center gap-x-4 border-b border-gray-900/5 bg-white p-6">
                      <Image
                        width={50}
                        height={50}
                        src="images/graficas.svg"
                        alt="Reform"
                        className="h-12 w-12 p-3 flex-none rounded-lg bg-white object-cover ring-1 ring-gray-900/10"
                      />
                      <div className="text-sm font-medium leading-6 text-gray-900">
                        Índice de cancelación
                      </div>
                      <div className="relative ml-auto">
                        <button
                          type="button"
                          className="-m-2.5 block p-2.5 text-gray-400 hover:text-gray-500"
                          id="options-menu-2-button"
                          aria-expanded="false"
                          aria-haspopup="true"
                        >
                          <span className="sr-only">Open options</span>
                          <svg
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path d="M3 10a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0zM8.5 10a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0zM15.5 8.5a1.5 1.5 0 100 3 1.5 1.5 0 000-3z" />
                          </svg>
                        </button>
                        {/*
            Dropdown menu, show/hide based on menu state.
  
            Entering: "transition ease-out duration-100"
              From: "transform opacity-0 scale-95"
              To: "transform opacity-100 scale-100"
            Leaving: "transition ease-in duration-75"
              From: "transform opacity-100 scale-100"
              To: "transform opacity-0 scale-95"
          */}
                        <div
                          className="absolute right-0 z-10 mt-0.5 w-32 origin-top-right rounded-md bg-white py-2 shadow-lg ring-1 ring-gray-900/5 focus:outline-none"
                          role="menu"
                          aria-orientation="vertical"
                          aria-labelledby="options-menu-2-button"
                          tabIndex={-1}
                        >
                          {/* Active: "bg-gray-50", Not Active: "" */}
                          <Link
                            href="/inicio"
                            className="block px-3 py-1 text-sm leading-6 text-gray-900"
                            role="menuitem"
                            tabIndex={-1}
                            id="options-menu-2-item-0"
                          >
                            Últimos 30 dias
                            <span className="sr-only">, Reform</span>
                          </Link>
                          <Link
                            href="/inicio"
                            className="block px-3 py-1 text-sm leading-6 text-gray-900"
                            role="menuitem"
                            tabIndex={-1}
                            id="options-menu-2-item-1"
                          >
                            Año<span className="sr-only">, Reform</span>
                          </Link>
                        </div>
                      </div>
                    </div>
                    <dl className="-my-3 bg-white divide-y divide-gray-100 px-6 py-4 text-sm leading-6">
                      <div className="flex justify-between gap-x-4 py-3">
                        <dt className="text-gray-500">
                          Índice general de cancelación
                        </dt>
                        <dd className="text-gray-700">
                          <time dateTime="2023-01-23">January 23, 2023</time>
                        </dd>
                      </div>
                      <div className="flex justify-between gap-x-4 py-3">
                        <dt className="text-gray-500">Porcentaje</dt>
                        <dd className="flex items-start gap-x-2">
                          <div className="font-medium text-gray-900">7%</div>
                        </dd>
                      </div>
                    </dl>
                  </li>
                </ul>
                {/* Nuevo componente TAILWIND aqui */}
                {/* Nuevo componente TAILWIND aqui */}
                {/* PANNEL BLANCO */}
                <div className="overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    {/* Nuevo componente TAILWIND aqui */}
                    {/* Nuevo componente TAILWIND aqui */}
                    {/* LISTA DE PACIENTES CON AVATAR */}
                    <div className="pl-2 pr-2">
                      <div className="sm:flex sm:items-center">
                        <div className="sm:flex-auto">
                          <h1 className="text-base font-semibold leading-6 text-gray-900">
                            Valoraciones recientes
                          </h1>
                        </div>
                        <div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
                          <button
                            type="button"
                            className="block rounded-md bg-gray-800 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-gray-700 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"
                          >
                            Ver todos
                          </button>
                        </div>
                      </div>
                      <div className="mt-8 flow-root">
                        <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                            <table className="min-w-full divide-y divide-gray-100">
                              <thead>
                                <tr>
                                  <th
                                    scope="col"
                                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                                  >
                                    Nombre del paciente
                                  </th>
                                  <th
                                    scope="col"
                                    className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                                  >
                                    Correo electrónico
                                  </th>
                                  <th
                                    scope="col"
                                    className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                                  >
                                    Calificación
                                  </th>
                                  <th
                                    scope="col"
                                    className="relative py-3.5 pl-3 pr-4 sm:pr-0"
                                  >
                                    <span className="sr-only">Edit</span>
                                  </th>
                                </tr>
                              </thead>
                              <tbody className="divide-y divide-gray-200 bg-white">
                                <tr>
                                  <td className="whitespace-nowrap py-5 pl-4 pr-3 text-sm sm:pl-0">
                                    <div className="flex items-center">
                                      <div className="h-11 w-11 flex-shrink-0">
                                        <Image
                                          width={50}
                                          height={50}
                                          className="h-11 w-11 rounded-full"
                                          src="https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                                          alt="image"
                                        />
                                      </div>
                                      <div className="ml-4">
                                        <div className="font-medium text-gray-900">
                                          Sofia Sanchez
                                        </div>
                                      </div>
                                    </div>
                                  </td>
                                  <td className="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <div className="mt-1 text-gray-500">
                                      sofia.sanchez@ejemplo.com
                                    </div>
                                  </td>
                                  <td className="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <span className="pr-2 inline-flex items-center rounded-md bg-gray-50 px-2 text-left py-1 text-xs font-medium text-gray-color ring-1 ring-inset ring-gray-600/20">
                                      <Image
                                        width={50}
                                        height={50}
                                        src="images/estrella.svg"
                                        className="h-3 pr-1 opacity-70"
                                        alt="image"
                                      />
                                      4.5 estrellas
                                    </span>
                                  </td>
                                  <td className="relative whitespace-nowrap py-5 pl-3 pr-4 text-right text-sm font-medium sm:pr-0">
                                    <Link
                                      href="/inicio"
                                      className="text-gray-600 hover:text-gray-900"
                                    >
                                      Leer comentario
                                      <span className="sr-only">
                                        , Lindsay Walton
                                      </span>
                                    </Link>
                                  </td>
                                </tr>
                                {/* Mas valoraciones de pacientes */}
                              </tbody>
                              <tbody className="divide-y divide-gray-200 bg-white">
                                <tr>
                                  <td className="whitespace-nowrap py-5 pl-4 pr-3 text-sm sm:pl-0">
                                    <div className="flex items-center">
                                      <div className="h-11 w-11 flex-shrink-0">
                                        <Image
                                          width={50}
                                          height={50}
                                          className="h-11 w-11 rounded-full"
                                          src="https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                                          alt="image"
                                        />
                                      </div>
                                      <div className="ml-4">
                                        <div className="font-medium text-gray-900">
                                          Sofia Sanchez
                                        </div>
                                      </div>
                                    </div>
                                  </td>
                                  <td className="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <div className="mt-1 text-gray-500">
                                      sofia.sanchez@ejemplo.com
                                    </div>
                                  </td>
                                  <td className="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <span className="pr-2 inline-flex items-center rounded-md bg-gray-50 px-2 text-left py-1 text-xs font-medium text-gray-color ring-1 ring-inset ring-gray-600/20">
                                      <Image
                                        width={50}
                                        height={50}
                                        src="images/estrella.svg"
                                        className="h-3 pr-1 opacity-70"
                                        alt="image"
                                      />
                                      4.5 estrellas
                                    </span>
                                  </td>
                                  <td className="relative whitespace-nowrap py-5 pl-3 pr-4 text-right text-sm font-medium sm:pr-0">
                                    <Link
                                      href="/inicio"
                                      className="text-gray-600 hover:text-gray-900"
                                    >
                                      Leer comentario
                                      <span className="sr-only">
                                        , Lindsay Walton
                                      </span>
                                    </Link>
                                  </td>
                                </tr>
                                {/* Mas valoraciones de pacientes aqui */}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    </PrivateRoute>
  );
};

export default Estadistica;

import React from "react";
import "tailwindcss/tailwind.css";
import { wrapper, store } from "@/redux/store";
import { Provider } from "react-redux";
import '@/styles/global.css'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}

export default wrapper.withRedux(MyApp);
import Sidebar from "@/components/Sidebar";
import Image from "next/image";
import PrivateRoute from "@/components/PrivateRoute";

const Perfil = () => {
  return (
    <>
      <PrivateRoute>
        <Sidebar />
        <main className="lg:pl-72  bg-gray-100">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-4">
              <div className="px-4 sm:px-6 lg:px-8">
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    <form className="md:col-span-2">
                      <div className="mb-8 col-span-full flex items-center gap-x-8">
                        <Image
                          width={50}
                          height={50}
                          src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                          alt=""
                          className="h-24 w-24 flex-none rounded-lg bg-gray-800 object-cover"
                        />
                        <div>
                          <button
                            type="button"
                            className="rounded-md bg-gray-700 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-gray-600"
                          >
                            Cambiar
                          </button>
                          <p className="mt-2 text-xs leading-5 text-gray-400">
                            JPG, GIF 0 PNG o hasta 1MB.
                          </p>
                        </div>
                      </div>
                      {/* Nuevo componente TAILWIND */}
                      {/* Nuevo componente TAILWIND */}
                      {/* INPUT TEXTO (NOMBRE COMPLETO) */}
                      <div className="flex flex-col mb-8 sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                        <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                          <label
                            htmlFor="name"
                            className="block text-xs font-medium text-gray-900"
                          >
                            Nombre Completo
                          </label>
                          <input
                            type="text"
                            name="name"
                            id="name"
                            className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder="Nombre - Apellido Paterno - Apellido Materno"
                          />
                        </div>
                        {/* COMBOBOX (TRATAMIENTO DR. DRA. PROF.) */}
                        <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                          <label
                            htmlFor="treatment"
                            className="block text-xs font-medium text-gray-900"
                          >
                            Tratamiento
                          </label>
                          <select
                            name="treatment"
                            id="treatment"
                            className="shadow-none block w-full border-0 p-0 text-gray-700 focus:ring-0 sm:text-sm sm:leading-6"
                          >
                            <option value="">Seleccione una opción</option>
                            <option value="Dr.">Dr.</option>
                            <option value="Dra.">Dra.</option>
                          </select>
                        </div>
                      </div>
                      {/* INPUT TEXTO (NO. DE CEDULA PROFESIONAL) */}
                      <div className="flex flex-col mb- sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                        <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                          <label
                            htmlFor="name"
                            className="block text-xs font-medium text-gray-900"
                          >
                            No. de Cédula Prof.
                          </label>
                          <input
                            type="text"
                            name="name"
                            id="name"
                            className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder={"00000000"}
                          />
                        </div>
                        {/* INPUT TEXTO (TELEFONO) */}
                        <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                          <label
                            htmlFor="name"
                            className="block text-xs font-medium text-gray-900"
                          >
                            Teléfono
                          </label>
                          <input
                            type="text"
                            name="name"
                            id="name"
                            className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder="123-4567-8910"
                          />
                        </div>
                      </div>
                      {/* Button at the bottom right */}
                      <div className="flex mt-4">
                        <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                          Actualizar cambios
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
                {/* Nuevo componente TAILWIND aqui */}
                {/* Nuevo componente TAILWIND aqui */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    <h3 className="ml-2 mb-4 pt-0 text-base font-semibold leading-6 text-gray-900">
                      Especialidades y Subespecialidades
                    </h3>
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* INPUT TEXTO (ESPECIALIDADES) */}
                    <div className="flex flex-col mb- sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Especialidad
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Escribe tu especialidad"
                        />
                      </div>
                      {/* INPUT TEXTO (SUBESPECIALIDADES) */}
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Subespecialidad
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Escribe tu subespecialidad"
                        />
                      </div>
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Nuevo componente TAILWIND aqui */}
                {/* Nuevo componente TAILWIND aqui */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    <h3 className="ml-2 pt-0 text-base font-semibold leading-6 text-gray-900">
                      Enfermedades Tratadas
                    </h3>
                    <h6 className="ml-2 mb-4 text-sm leading-6 text-gray-400">
                      Frecuentemente, los pacientes buscan a especialistas
                      tecleando una dolencia en el campo de búsqueda.
                    </h6>
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* INPUT TEXTO (ENFERMEDAD) */}
                    <div className="flex flex-col mb-8 sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Enfermedad
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Escribe una enfermedad"
                        />
                      </div>
                      {/* INPUT TEXTO (ENFERMEDAD) */}
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Enfermedad
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Escribe una enfermedad"
                        />
                      </div>
                    </div>
                    {/* INPUT TEXTO (ENFERMEDAD) */}
                    <div className="flex flex-col sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Enfermedad
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Escribe una enfermedad"
                        />
                      </div>
                      {/* INPUT TEXTO (ENFERMEDAD) */}
                      <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                        <label
                          htmlFor="name"
                          className="block text-xs font-medium text-gray-900"
                        >
                          Enfermedad
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                          placeholder="Escribe una enfermedad"
                        />
                      </div>
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Nuevo componente TAILWIND */}
                {/* Nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    <h3 className="ml-2 mb-4 text-base font-semibold leading-6 text-gray-900">
                      Formación
                    </h3>
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* INPUT TEXTO (FORMACION) */}
                    <div>
                      <textarea
                        rows={4}
                        name="comment"
                        id="comment"
                        className="block w-full rounded-md pl-3 border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-gray-600 sm:text-sm sm:leading-6"
                        placeholder="Para una gran cantidad de pacientes, la educación médica tiene tanta relevancia como la experiencia laboral."
                        defaultValue={""}
                      />
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Nuevo componente TAILWIND */}
                {/* Nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* INPUT TEXTO (PREMIOS Y DISTINCIONES) */}
                    <h3 className="ml-2 mb-4 text-base font-semibold leading-6 text-gray-900">
                      Premios y Distinciones
                    </h3>
                    <div>
                      <textarea
                        rows={4}
                        name="comment"
                        id="comment"
                        className="block w-full rounded-md pl-3 border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-gray-600 sm:text-sm sm:leading-6"
                        placeholder="¿Has recibido algún galardón o mérito especial? ¡A tus pacientes les entusiasmará conocer esta información!"
                        defaultValue={""}
                      />
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Nuevo componente TAILWIND */}
                {/* Nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    <h3 className="ml-2 mb-6 text-base font-semibold leading-6 text-gray-900">
                      Idiomas
                    </h3>
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* COMBOBOX (IDIOMAS) */}
                    <div>
                      <div className="mb-8 relative mt-2">
                        <input
                          id="combobox"
                          type="text"
                          className="w-full rounded-md border-0 bg-white py-1.5 pl-3 pr-12 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          role="combobox"
                          aria-controls="options"
                          aria-expanded="false"
                        />
                        <button
                          type="button"
                          className="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none"
                        >
                          <svg
                            className="h-5 w-5 text-gray-400"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 3a.75.75 0 01.55.24l3.25 3.5a.75.75 0 11-1.1 1.02L10 4.852 7.3 7.76a.75.75 0 01-1.1-1.02l3.25-3.5A.75.75 0 0110 3zm-3.76 9.2a.75.75 0 011.06.04l2.7 2.908 2.7-2.908a.75.75 0 111.1 1.02l-3.25 3.5a.75.75 0 01-1.1 0l-3.25-3.5a.75.75 0 01.04-1.06z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </button>
                        <ul
                          className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm"
                          id="options"
                          role="listbox"
                        >
                          {/*
                Combobox option, manage highlight styles based on mouseenter/mouseleave and keyboard navigation.
        
                Active: "text-white bg-indigo-600", Not Active: "text-gray-900"
              */}
                          <li
                            className="relative cursor-default select-none py-2 pl-3 pr-9 text-gray-900"
                            id="option-0"
                            role="option"
                            tabIndex={-1}
                          >
                            {/* Selected: "font-semibold" */}
                            <span className="block truncate">
                              Leslie Alexander
                            </span>
                            {/*
                  Checkmark, only display for selected option.
        
                  Active: "text-white", Not Active: "text-indigo-600"
                */}
                            <span className="absolute inset-y-0 right-0 flex items-center pr-4 text-custom-color">
                              <svg
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                                aria-hidden="true"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            </span>
                          </li>
                          {/* More items... */}
                        </ul>
                      </div>
                    </div>
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* COMBOBOX (IDIOMAS) */}
                    <div>
                      <div className="mb-8 relative mt-2">
                        <input
                          id="combobox"
                          type="text"
                          className="w-full rounded-md border-0 bg-white py-1.5 pl-3 pr-12 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          role="combobox"
                          aria-controls="options"
                          aria-expanded="false"
                        />
                        <button
                          type="button"
                          className="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none"
                        >
                          <svg
                            className="h-5 w-5 text-gray-400"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 3a.75.75 0 01.55.24l3.25 3.5a.75.75 0 11-1.1 1.02L10 4.852 7.3 7.76a.75.75 0 01-1.1-1.02l3.25-3.5A.75.75 0 0110 3zm-3.76 9.2a.75.75 0 011.06.04l2.7 2.908 2.7-2.908a.75.75 0 111.1 1.02l-3.25 3.5a.75.75 0 01-1.1 0l-3.25-3.5a.75.75 0 01.04-1.06z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </button>
                        <ul
                          className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm"
                          id="options"
                          role="listbox"
                        >
                          {/*
                Combobox option, manage highlight styles based on mouseenter/mouseleave and keyboard navigation.
        
                Active: "text-white bg-indigo-600", Not Active: "text-gray-900"
              */}
                          <li
                            className="relative cursor-default select-none py-2 pl-3 pr-9 text-gray-900"
                            id="option-0"
                            role="option"
                            tabIndex={-1}
                          >
                            {/* Selected: "font-semibold" */}
                            <span className="block truncate">
                              Leslie Alexander
                            </span>
                            {/*
                  Checkmark, only display for selected option.
        
                  Active: "text-white", Not Active: "text-indigo-600"
                */}
                            <span className="absolute inset-y-0 right-0 flex items-center pr-4 text-custom-color">
                              <svg
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                                aria-hidden="true"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            </span>
                          </li>
                          {/* More items... */}
                        </ul>
                      </div>
                    </div>
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* COMBOBOX (IDIOMAS) */}
                    <div>
                      <div className="relative mt-2">
                        <input
                          id="combobox"
                          type="text"
                          className="w-full rounded-md border-0 bg-white py-1.5 pl-3 pr-12 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          role="combobox"
                          aria-controls="options"
                          aria-expanded="false"
                        />
                        <button
                          type="button"
                          className="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none"
                        >
                          <svg
                            className="h-5 w-5 text-gray-400"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 3a.75.75 0 01.55.24l3.25 3.5a.75.75 0 11-1.1 1.02L10 4.852 7.3 7.76a.75.75 0 01-1.1-1.02l3.25-3.5A.75.75 0 0110 3zm-3.76 9.2a.75.75 0 011.06.04l2.7 2.908 2.7-2.908a.75.75 0 111.1 1.02l-3.25 3.5a.75.75 0 01-1.1 0l-3.25-3.5a.75.75 0 01.04-1.06z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </button>
                        <ul
                          className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm"
                          id="options"
                          role="listbox"
                        >
                          {/*
                Combobox option, manage highlight styles based on mouseenter/mouseleave and keyboard navigation.
        
                Active: "text-white bg-indigo-600", Not Active: "text-gray-900"
              */}
                          <li
                            className="relative cursor-default select-none py-2 pl-3 pr-9 text-gray-900"
                            id="option-0"
                            role="option"
                            tabIndex={-1}
                          >
                            {/* Selected: "font-semibold" */}
                            <span className="block truncate">
                              Leslie Alexander
                            </span>
                            {/*
                  Checkmark, only display for selected option.
        
                  Active: "text-white", Not Active: "text-indigo-600"
                */}
                            <span className="absolute inset-y-0 right-0 flex items-center pr-4 text-custom-color">
                              <svg
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                                aria-hidden="true"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            </span>
                          </li>
                          {/* More items... */}
                        </ul>
                      </div>
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
                {/* Nuevo componente TAILWIND */}
                {/* Nuevo componente TAILWIND */}
                {/* PANNEL BLANCO */}
                <div className="overflow-hidden rounded-lg bg-white shadow">
                  <div className="px-4 py-5 sm:p-6">
                    {/* Nuevo componente TAILWIND */}
                    {/* Nuevo componente TAILWIND */}
                    {/* IMAGEN (MEDIA) */}
                    <h3 className="ml-2 mb-4 text-base font-semibold leading-6 text-gray-900">
                      Media y Certificados
                    </h3>
                    <div className="grid grid-cols-4 gap-6">
                      <div className="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                        <div className="text-center">
                          <path fillRule="evenodd" clipRule="evenodd">
                            <div className="mt-4 flex flex-col sm:flex-row text-sm leading-6 text-gray-600">
                              <label
                                htmlFor="file-upload"
                                className="relative cursor-pointer rounded-md bg-white font-semibold text-cyan-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-gray-500 mb-2 sm:mb-0 sm:mr-2"
                              >
                                <span>Buscar archivo</span>
                                <input
                                  id="file-upload"
                                  name="file-upload"
                                  type="file"
                                  className="sr-only"
                                />
                              </label>
                              <p />
                            </div>
                            <p className="text-xs leading-5 text-gray-600">
                              Arrastra tu archivo
                            </p>
                          </path>
                        </div>
                      </div>
                      {/* IMAGEN */}
                      <div className="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                        <div className="text-center">
                          <path fillRule="evenodd" clipRule="evenodd">
                            <div className="mt-4 flex flex-col sm:flex-row text-sm leading-6 text-gray-600">
                              <label
                                htmlFor="file-upload"
                                className="relative cursor-pointer rounded-md bg-white font-semibold text-cyan-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-gray-500 mb-2 sm:mb-0 sm:mr-2"
                              >
                                <span>Buscar archivo</span>
                                <input
                                  id="file-upload"
                                  name="file-upload"
                                  type="file"
                                  className="sr-only"
                                />
                              </label>
                              <p />
                            </div>
                            <p className="text-xs leading-5 text-gray-600">
                              Arrastra tu archivo
                            </p>
                          </path>
                        </div>
                      </div>
                      {/* IMAGEN */}
                      <div className="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                        <div className="text-center">
                          <path fillRule="evenodd" clipRule="evenodd">
                            <div className="mt-4 flex flex-col sm:flex-row text-sm leading-6 text-gray-600">
                              <label
                                htmlFor="file-upload"
                                className="relative cursor-pointer rounded-md bg-white font-semibold text-cyan-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-gray-500 mb-2 sm:mb-0 sm:mr-2"
                              >
                                <span>Buscar archivo</span>
                                <input
                                  id="file-upload"
                                  name="file-upload"
                                  type="file"
                                  className="sr-only"
                                />
                              </label>
                              <p />
                            </div>
                            <p className="text-xs leading-5 text-gray-600">
                              Arrastra tu archivo
                            </p>
                          </path>
                        </div>
                      </div>
                      {/* IMAGEN */}
                      <div className="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                        <div className="text-center">
                          <path fillRule="evenodd" clipRule="evenodd">
                            <div className="mt-4 flex flex-col sm:flex-row text-sm leading-6 text-gray-600">
                              <label
                                htmlFor="file-upload"
                                className="relative cursor-pointer rounded-md bg-white font-semibold text-cyan-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-gray-500 mb-2 sm:mb-0 sm:mr-2"
                              >
                                <span>Buscar archivo</span>
                                <input
                                  id="file-upload"
                                  name="file-upload"
                                  type="file"
                                  className="sr-only"
                                />
                              </label>
                              <p />
                            </div>
                            <p className="text-xs leading-5 text-gray-600">
                              Arrastra tu archivo
                            </p>
                          </path>
                        </div>
                      </div>
                    </div>
                    {/* Button at the bottom right */}
                    <div className="flex mt-4">
                      <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                        Actualizar cambios
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </PrivateRoute>
    </>
  );
};

export default Perfil;

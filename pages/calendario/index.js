import Sidebar from "@/components/Sidebar";
import Link from "next/link";
import Image from "next/image";
import PrivateRoute from "@/components/PrivateRoute";

const Calendario = () => {
  return (
    <>
      <PrivateRoute>
        <Sidebar />
        <main className="lg:pl-72  bg-gray-100">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-4">
              <div className="px-4 sm:px-6 lg:px-8 flex flex-col md:flex-row justify-start">
                <div className="w-full min-w-[300px] md:w-3/6 h-3/4 overflow-hidden rounded-lg bg-white shadow p-8 mb-8 md:mb-0">
                  <div className="text-center">
                    <div className="flex items-center text-gray-900 justify-center">
                      <button
                        type="button"
                        className="-m-1.5 flex flex-none items-center justify-center p-1.5 text-gray-400 hover:text-gray-500"
                      >
                        <span className="sr-only">Previous month</span>
                        <svg
                          className="h-5 w-5"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            fillRule="evenodd"
                            d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </button>
                      <div className="flex-auto text-sm font-semibold">
                        Enero
                      </div>
                      <button
                        type="button"
                        className="-m-1.5 flex flex-none items-center justify-center p-1.5 text-gray-400 hover:text-gray-500"
                      >
                        <span className="sr-only">Next month</span>
                        <svg
                          className="h-5 w-5"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            fillRule="evenodd"
                            d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </button>
                    </div>
                    <div className="mt-6 grid grid-cols-7 text-xs leading-6 text-gray-500">
                      <div>Lun</div>
                      <div>Mar</div>
                      <div>Mie</div>
                      <div>Jue</div>
                      <div>Vie</div>
                      <div>Sab</div>
                      <div>Dom</div>
                    </div>
                    <div className="h-72 isolate mt-2 grid grid-cols-7 gap-px rounded-lg bg-gray-200 text-sm shadow ring-1 ring-gray-200">
                      {/*
                Always include: "py-1.5 hover:bg-gray-100 focus:z-10"
                Is current month, include: "bg-white"
                Is not current month, include: "bg-gray-50"
                Is selected or is today, include: "font-semibold"
                Is selected, include: "text-white"
                Is not selected, is not today, and is current month, include: "text-gray-900"
                Is not selected, is not today, and is not current month, include: "text-gray-400"
                Is today and is not selected, include: "text-indigo-600"
      
                Top left day, include: "rounded-tl-lg"
                Top right day, include: "rounded-tr-lg"
                Bottom left day, include: "rounded-bl-lg"
                Bottom right day, include: "rounded-br-lg"
              */}
                      <button
                        type="button"
                        className="rounded-tl-lg bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        {/*
                  Always include: "mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                  Is selected and is today, include: "bg-indigo-600"
                  Is selected and is not today, include: "bg-gray-900"
                */}
                        <time
                          dateTime="2021-12-27"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          27
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2021-12-28"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          28
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2021-12-29"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          29
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2021-12-30"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          30
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2021-12-31"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          31
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-01"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          1
                        </time>
                      </button>
                      <button
                        type="button"
                        className="rounded-tr-lg bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-01"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          2
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-02"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          3
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-04"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          4
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-05"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          5
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-06"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          6
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-07"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          7
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-08"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          8
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-09"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          9
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-10"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          10
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-11"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          11
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 font-semibold text-indigo-600 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-12"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          12
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-13"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          13
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-14"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          14
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-15"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          15
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-16"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          16
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-17"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          17
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-18"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          18
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-19"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          19
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-20"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          20
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-21"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          21
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-22"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full bg-gray-900 font-semibold text-white"
                        >
                          22
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-23"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          23
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-24"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          24
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-25"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          25
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-26"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          26
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-27"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          27
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-28"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          28
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-29"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          29
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-30"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          30
                        </time>
                      </button>
                      <button
                        type="button"
                        className="rounded-bl-lg bg-white py-1.5 text-gray-900 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-01-31"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          31
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-02-01"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          1
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-02-02"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          2
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-02-03"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          3
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-02-04"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          4
                        </time>
                      </button>
                      <button
                        type="button"
                        className="bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-02-05"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          5
                        </time>
                      </button>
                      <button
                        type="button"
                        className="rounded-br-lg bg-gray-50 py-1.5 text-gray-400 hover:bg-gray-100 focus:z-10"
                      >
                        <time
                          dateTime="2022-02-06"
                          className="mx-auto flex h-7 w-7 items-center justify-center rounded-full"
                        >
                          6
                        </time>
                      </button>
                    </div>
                  </div>
                </div>
                {/* Nuevo componente Tailwind aqui */}
                {/* Nuevo componente Tailwind aqui */}
                {/* LISTA DE PACIENTES */}
                <div className="w-full ml-0 md:ml-8">
                  <div className="sm:flex sm:items-center">
                    <div className="sm:flex-auto"></div>
                  </div>
                  <div>
                    <div className="-mx-4 -my-2 mb-8 overflow-x-auto sm:-mx-6 lg:-mx-8">
                      <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                        <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 sm:rounded-lg">
                          <table className="min-w-full divide-y divide-gray-100">
                            <thead className="bg-gray-50">
                              <tr>
                                <th
                                  scope="col"
                                  className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6"
                                >
                                  Nombre
                                </th>
                                <th
                                  scope="col"
                                  className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                                >
                                  Hora
                                </th>
                                <th
                                  scope="col"
                                  className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                                >
                                  Dirección / Consultorio
                                </th>
                                <th
                                  scope="col"
                                  className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                                >
                                  Motivo
                                </th>
                                <th
                                  scope="col"
                                  className="relative py-3.5 pl-3 pr-4 sm:pr-6"
                                >
                                  <span className="sr-only">Edit</span>
                                </th>
                              </tr>
                            </thead>
                            <tbody className="divide-y divide-gray-100 bg-white">
                              <tr>
                                <td className="whitespace-nowrap py-4 pl-4 text-sm font-medium text-gray-900 sm:pl-6 flex items-center space-x-2">
                                  <Image
                                    alt="image"
                                    width={50}
                                    height={50}
                                    src="/images/avatar_33.svg"
                                    className="h-8 w-8 mr-2 rounded-full overflow-hidden"
                                  />
                                  <span>Lindsay Walton</span>
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  10:00 AM
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  Smile Dental/Ciudad de Mexico
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  Primera visita
                                </td>
                                <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                  <Link
                                    href="/inicio"
                                    className="text-indigo-600 hover:text-indigo-900"
                                  >
                                    Opciones
                                    <span className="sr-only">
                                      , Lindsay Walton
                                    </span>
                                  </Link>
                                </td>
                              </tr>
                              {/* More people... */}
                              <tr>
                                <td className="whitespace-nowrap py-4 pl-4 text-sm font-medium text-gray-900 sm:pl-6 flex items-center space-x-2">
                                  <Image
                                    alt="image"
                                    width={50}
                                    height={50}
                                    src="/images/avatar_33.svg"
                                    className="h-8 w-8 mr-2 rounded-full overflow-hidden"
                                  />
                                  <span>Lindsay Walton</span>
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  10:30 AM
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  Smile Dental/Ciudad de Mexico
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  Primera visita
                                </td>
                                <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                  <Link
                                    href="/opciones"
                                    className="text-indigo-600 hover:text-indigo-900"
                                  >
                                    Opciones
                                    <span className="sr-only">
                                      , Lindsay Walton
                                    </span>
                                  </Link>
                                </td>
                              </tr>
                              {/* More people... */}
                              <tr>
                                <td className="whitespace-nowrap py-4 pl-4 text-sm font-medium text-gray-900 sm:pl-6 flex items-center space-x-2">
                                  <Image
                                    alt="image"
                                    width={50}
                                    height={50}
                                    src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                                    className="h-8 w-8 mr-2 rounded-full overflow-hidden"
                                  />
                                  <span>Lindsay Walton</span>
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  11:00 AM
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  Smile Dental/Ciudad de Mexico
                                </td>
                                <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                  Visita subsecuente
                                </td>
                                <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                  <Link
                                    href="/opciones"
                                    className="text-indigo-600 hover:text-indigo-900"
                                  >
                                    Opciones
                                    <span className="sr-only">
                                      , Lindsay Walton
                                    </span>
                                  </Link>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </PrivateRoute>
    </>
  );
};

export default Calendario;

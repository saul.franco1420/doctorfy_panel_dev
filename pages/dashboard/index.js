import React from "react";
import PrivateRoute from "@/components/PrivateRoute";
import Sidebar from "@/components/Sidebar";


const Index = () => {
  return (
    <>
      <PrivateRoute>
        <Sidebar />
        <main className="lg:pl-72">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-5">
              <iframe
                src="https://doctorfy-dev.netlify.app/"
                title="Doctorfy App"
                width="100%"
                height="785"
                allowFullScreen
                style={{
                  borderRadius: "25px",
                }}
              ></iframe>
            </div>
          </div>
        </main>
      </PrivateRoute>
    </>
  );
};

export default Index;

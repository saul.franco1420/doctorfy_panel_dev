import Sidebar from "@/components/Sidebar";
import PrivateRoute from "@/components/PrivateRoute";
import Link from "next/link";

const Cuenta = () => {
  return (
    <>
      <PrivateRoute>
        <Sidebar />
        <main className="lg:pl-72  bg-gray-100">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-4">
              <div className="px-4 sm:px-6 lg:px-8">
                <div className="lg:grid lg:grid-cols-12 lg:gap-x-5">
                  <aside className="px-2 py-6 sm:px-6 lg:col-span-3 lg:px-0 lg:py-0">
                    <nav className="space-y-1">
                      <Link
                        href="/inicio"
                        className="bg-gray-50 text-custom-color hover:bg-white hover:text-custom-color group flex items-center rounded-md px-3 py-2 text-sm font-medium"
                        aria-current="page"
                      >
                        <svg
                          className="text-custom-color -ml-1 mr-3 h-6 w-6 flex-shrink-0"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                          />
                        </svg>
                        <span className="truncate">Cuenta</span>
                      </Link>
                      <Link
                        href="/metodo_pago"
                        className="text-gray-900 hover:bg-gray-50 hover:text-custom-color group flex items-center rounded-md px-3 py-2 text-sm font-medium"
                      >
                        <svg
                          className="text-gray-400 group-hover:text-custom-color -ml-1 mr-3 h-6 w-6 flex-shrink-0"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z"
                          />
                        </svg>
                        <span className="truncate">Metodo de pago</span>
                      </Link>
                    </nav>
                  </aside>
                  <div className="space-y-6 sm:px-6 lg:col-span-9 lg:px-0">
                    <form action="#" method="POST">
                      <div className="sm:overflow-hidden sm:rounded-md">
                        <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                          <div className="px-4 py-5 sm:p-6">
                            <h3 className="ml-2 pt-0 text-base font-semibold leading-6 text-gray-900">
                              Configuración de la cuenta
                            </h3>
                            <h6 className="ml-2 mb-4 text-sm leading-6 text-gray-400">
                              Esta información es privada y no se mostrará al
                              público.
                            </h6>
                            {/* INPUT TEXTO (NOMBRE) */}
                            <div className="flex flex-col mb-8 sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                              <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                                <label
                                  htmlFor="name"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Nombre
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Campo obligatorio"
                                />
                              </div>
                              {/* INPUT (APELLIDO) */}
                              <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                                <label
                                  htmlFor="treatment"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Apellido
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Campo obligatorio"
                                />
                              </div>
                            </div>
                            {/* INPUT TEXTO (CORREO ELECTRONICO) */}
                            <div className="flex flex-col mb- sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                              <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                                <label
                                  htmlFor="name"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Correo electrónico
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Campo obligatorio"
                                />
                              </div>
                              {/* INPUT TEXTO (TELEFONO) */}
                              <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                                <label
                                  htmlFor="name"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Teléfono
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Campo obligatorio"
                                />
                              </div>
                            </div>
                            {/* Button at the bottom right */}
                            <div className="flex mt-4">
                              <button className="ml-auto mt-2 rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600">
                                Actualizar cambios
                              </button>
                            </div>
                          </div>
                        </div>

                        <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                          <div className="px-4 py-5 sm:p-6">
                            <h3 className="ml-2 mb-4 pt-0 text-base font-semibold leading-6 text-gray-900">
                              Cambiar nombre de usuario
                            </h3>

                            <div className="flex flex-col space-y-4">
                              {/* INPUT */}
                              <div className="mb-2 rounded-md w-full px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                                <label
                                  htmlFor="name"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Nombre de usuario
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Campo obligatorio"
                                />
                              </div>
                              <button
                                type="button"
                                className="ml-auto rounded-md bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"
                              >
                                Cambiar usuario
                              </button>
                            </div>
                          </div>
                        </div>

                        <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                          <div className="px-4 py-5 sm:p-6">
                            <h3 className="ml-2 pt-0 text-base font-semibold leading-6 text-gray-900">
                              Conraseña
                            </h3>
                            <h6 className="ml-2 mb-4 text-sm leading-6 text-gray-400">
                              Actualiza tu contraseña asociada a esta cuenta.
                            </h6>

                            <div className="flex flex-col mb-8 sm:flex-row space-y-4 sm:space-y-0 sm:space-x-5 justify-between">
                              <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                                <label
                                  htmlFor="name"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Contraseña actual
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Ingresa tu contraseña actual"
                                />
                              </div>
                              {/* INPUT TEXTO (CONTRASENA NUEVA) */}
                              <div className="rounded-md w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800">
                                <label
                                  htmlFor="name"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Contraseña nueva
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Ingresa tu nueva contraseña"
                                />
                              </div>
                            </div>
                            {/* INPUT TEXTO (CONFIRMACION DE CONTRASENA) */}
                            <div className="flex flex-col md:flex-row items-center justify-between space-y-4 md:space-y-0 md:space-x-6">
                              <div className="rounded-md mr-auto w-full sm:w-1/2 px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset ring-gray-200 focus-within:ring-2 focus-within:ring-gray-800 mb-4 md:mb-0">
                                <label
                                  htmlFor="name"
                                  className="block text-xs font-medium text-gray-900"
                                >
                                  Confirmación de contraseña nueva
                                </label>
                                <input
                                  type="text"
                                  name="name"
                                  id="name"
                                  className="block w-full border-0 p-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                  placeholder="Confirma tu contraseña nueva"
                                />
                              </div>
                              {/* Added ml-auto class to the button */}
                              <button
                                type="button"
                                className="ml-auto rounded-md align-bottom bg-gray-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-gray-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"
                              >
                                Cambiar contraseña
                              </button>
                            </div>
                          </div>
                        </div>
                        {/* Nuevo componente TAILWIND */}
                        {/* Nuevo componente TAILWIND */}
                        {/* PANNEL BLANCO */}
                        <div className="mb-8 overflow-hidden rounded-lg bg-white shadow">
                          <div className="px-4 py-5 sm:p-6">
                            <h3 className="ml-2 mb-1 text-base font-semibold leading-6 text-gray-900">
                              Eliminar mi cuenta
                            </h3>
                            <h6 className="ml-2 mb-4 text-sm leading-6 text-gray-400">
                              Al eliminar tu cuenta, perderas acceso a todos los
                              servicios y beneficios que esta cuenta
                              proporciona. Toda infromación personal y pública
                              sera borrada de manera permanente.
                            </h6>

                            <div className="flex flex-col md:flex-row items-center space-y-4 md:space-y-0 md:space-x-4">
                              {" "}
                              {/* Adjusted the parent div */}
                              <fieldset className="ml-2 justify-start flex-grow">
                                <legend className="sr-only">
                                  Notifications
                                </legend>
                                <div className="space-y-5">
                                  <div className="relative flex items-start">
                                    <div className="flex h-6 items-center">
                                      <input
                                        id="comments"
                                        aria-describedby="comments-description"
                                        name="comments"
                                        type="checkbox"
                                        className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                      />
                                    </div>
                                    <div className="ml-3 text-sm leading-6">
                                      <label
                                        htmlFor="comments"
                                        className="font-medium text-gray-900"
                                      >
                                        ¿Estas seguro de que deseas eliminar tu
                                        cuenta?
                                      </label>
                                      <p
                                        id="comments-description"
                                        className="text-gray-500"
                                      >
                                        Toda información sera perdida de manera
                                        permanente.
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                              <button
                                type="button"
                                className="rounded-md bg-rose-500 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-rose-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"
                              >
                                Eliminar cuenta
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </PrivateRoute>
    </>
  );
};

export default Cuenta;

import clientAxios from "@/config/axios";
import {
  LOGIN_ACCESS_ERROR,
  LOGIN_ACCESS_LOADING,
  LOGIN_ACCESS_SUCCESS,
  LOGOUT_SUCCESS,
  VERIFY_LOGIN_ERROR,
  VERIFY_LOGIN_LOADING,
  VERIFY_LOGIN_SUCCESS,
} from "../type/login";

// Actions to get all Login
export const initAccessLogin = () => ({
  type: LOGIN_ACCESS_LOADING,
});

export const successAccessLogin = (data) => ({
  type: LOGIN_ACCESS_SUCCESS,
  payload: data,
});

export const errorAccessLogin = (error) => ({
  type: LOGIN_ACCESS_ERROR,
  payload: error,
});

// Asynchronous action to create Login
export function accessLogin(data) {
  return async (dispatch) => {
    dispatch(initAccessLogin());
    try {
      // Make POST request to the API with the provided data as the request body
      const response = await clientAxios.post(`/login/access`, data);
      // Dispatch success action with the retrieved data
      dispatch(successAccessLogin(response?.data?.data));
    } catch (error) {
      // Dispatch error action and display an alert
      console.error("Error: ", error.response.data);
      dispatch(errorAccessLogin(error.response.data));
    }
  };
}

// This part exports a function called 'successLogout' that returns an object.
export const successLogout = () => ({
  type: LOGOUT_SUCCESS,
});

// This part exports a function called 'Logout'.
export function Logout() {
  return async (dispatch) => {
    try {
      // When this async action is dispatched, it calls the 'successLogout' action creator
      dispatch(successLogout());
    } catch (error) {
      // If an error occurs during the process, it is caught here and logged to the console.
      console.error("Error: ", error);
    }
  };
}

// Actions to get verify login
export const verifyLoadingLogin = () => ({
  type: VERIFY_LOGIN_LOADING,
});

export const verifyAccessLogin = (data) => ({
  type: VERIFY_LOGIN_SUCCESS,
  payload: data,
});

export const verifyErrorLogin = (error) => ({
  type: VERIFY_LOGIN_ERROR,
  payload: error,
});

// Asynchronous action to verify Login
export function verifyLogin(data) {
  return async (dispatch) => {
    dispatch(verifyLoadingLogin());
    try {
      // Make POST request to the API with the provided data as the request body
      const response = await clientAxios.post(`/login/verify_access`, {
        access_token: data,
      });

      // Dispatch success action with the retrieved data
      dispatch(verifyAccessLogin(response.data));
    } catch (error) {
      // Dispatch error action and display an alert
      console.error("Error: ", error.response.data);
      dispatch(verifyErrorLogin(error.response.data));
    }
  };
}

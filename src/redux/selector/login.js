import { useSelector } from 'react-redux';

export const StatusLogin = () => {
  return useSelector((state) => ({
    loadingLogin: state.login.loading.login,
    loadingVerify: state.login.loading.verify,
    login: state.login.login,
    errorLogin: state.login.error.login,
    errorVerify: state.login.error.verify,
    profile: state.login.profile.data,
  }));
};

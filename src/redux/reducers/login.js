import {
  LOGIN_ACCESS_ERROR,
  LOGIN_ACCESS_LOADING,
  LOGIN_ACCESS_SUCCESS,
  LOGOUT_SUCCESS,
  VERIFY_LOGIN_ERROR,
  VERIFY_LOGIN_LOADING,
  VERIFY_LOGIN_SUCCESS,
} from "../type/login";

const initialState = {
  login: "",
  profile: {},
  loading: {
    login: false,
    verify: false
  },
  error: {
    login: false,
    verify: false
  },
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_ACCESS_LOADING:
      return {
        ...state,
        loading: {
          ...state.loading,
          login: true,
        },
        error: {
          ...state.error,
          login: false,
        },
      };

    case LOGIN_ACCESS_ERROR:
      return {
        ...state,
        loading: {
          ...state.loading,
          login: false,
        },
        error: {
          ...state.error,
          login: action.payload,
        },
      };

    case LOGIN_ACCESS_SUCCESS:
      localStorage.setItem("login", action.payload);
      return {
        ...state,
        loading: {
          ...state.loading,
          login: false,
        },
        error: {
          ...state.error,
          login: false,
        },
        login: action.payload,
      };

    case LOGOUT_SUCCESS:
      localStorage.removeItem("login");
      return {
        ...state,
        login: "",
        loading: {
          ...state.loading,
          login: false,
        },
        error: {
          ...state.error,
          login: false,
        },
      };

    case VERIFY_LOGIN_LOADING:
      return {
        ...state,
        loading: {
          ...state.loading,
          verify: true,
        },
        error: {
          ...state.error,
          verify: false,
        },
      };

    case VERIFY_LOGIN_ERROR:
      return {
        ...state,
        loading: {
          ...state.loading,
          verify: false,
        },
        error: {
          login: false,
          verify: action.payload,
        },
      };

    case VERIFY_LOGIN_SUCCESS:
      return {
        ...state,
        loading: {
          ...state.loading,
          verify: false,
        },
        error: {
          ...state.error,
          verify: false,
        },
        profile: action.payload,
      };

    default:
      return state;
  }
};

export default loginReducer;

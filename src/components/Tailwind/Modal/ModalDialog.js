import React, { Fragment } from 'react';
import { Transition } from '@headlessui/react';
import ModalContent from '@/components/ModalContent/VerifyUser';

const ModalDialog = ({ show }) => {
  return (
    <Transition.Root show={show} as={Fragment}>
      <Transition.Child
        as={Fragment}
        enter="ease-out duration-300"
        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
        enterTo="opacity-100 translate-y-0 sm:scale-100"
        leave="ease-in duration-200"
        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
      >
        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <ModalContent />
          </div>
        </div>
      </Transition.Child>
    </Transition.Root>
  );
};

export default ModalDialog;

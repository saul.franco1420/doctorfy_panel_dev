import { Fragment, useRef, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import VerifyUser from '@/components/ModalContent/VerifyUser';


export default function ModalSingleAction() {

    const [open, setOpen]   = useState(true)
    const cancelButtonRef   = useRef(null)


  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as="div" className="relative z-50" initialFocus={cancelButtonRef} onClose={() => {}}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>
        <VerifyUser/>
      </Dialog>
    </Transition.Root>
  )
}

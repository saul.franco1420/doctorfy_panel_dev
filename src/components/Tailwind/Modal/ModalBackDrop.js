import React from 'react';
import { Transition } from '@headlessui/react';

const ModalBackdrop = ({ show }) => {
  return (
    <Transition.Root show={show} as={React.Fragment}>
      <Transition.Child
        as={React.Fragment}
        enter="ease-out duration-300"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="ease-in duration-200"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
      </Transition.Child>
    </Transition.Root>
  );
};

export default ModalBackdrop;

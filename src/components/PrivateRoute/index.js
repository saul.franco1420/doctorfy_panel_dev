import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { verifyLogin } from "@/redux/actions/login";
import { useDispatch } from "react-redux";
import { StatusLogin } from "@/redux/selector/login";
import ModalSingleAction from "@/components/Tailwind/Modal/ModalSingleAction";
import ReactLoading from "react-loading";

const PrivateRoute = ({ children }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { login, profile, loadingVerify } = StatusLogin();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loadingDone, setLoadingDone] = useState(false);

  useEffect(() => {
    if (loadingDone) {
      const token = login || sessionStorage.getItem("login");
      const currentPath = router.pathname;

      if (currentPath !== "/login") {
        // Verify the login status first
        dispatch(verifyLogin(token));
      }

      // Once the login status is verified, decide whether to redirect
      if (!token) router.push("/login");
    }
  }, [login, loadingDone]);

  useEffect(() => {
    // Check the profile.role and update the modal and interaction states accordingly
    if (profile?.role === "paciente") {
      setIsModalOpen(true);
    } else {
      setIsModalOpen(false);
    }
  }, [profile]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoadingDone(true);
    }, 1500); // 1500 ms = 1.5 segundos

    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <>
      {loadingDone && !loadingVerify ? (
        <>
          {children}
          {isModalOpen && <ModalSingleAction />}
        </>
      ) : (
        <div
          className="flex items-center justify-center h-full"
          style={{ height: "100vh" }}
        >
          <ReactLoading type={"spin"} color={"#000"} height={50} width={50} />
        </div>
      )}
    </>
  );
};

export default PrivateRoute;

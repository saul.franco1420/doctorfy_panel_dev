import {
  CalendarIcon,
  ChartPieIcon,
  DocumentDuplicateIcon,
  FolderIcon,
  HomeIcon,
  UsersIcon,
} from "@heroicons/react/24/outline";

export const navigation = (currentNavItem) => {
  return [
    {
      name: "Inicio",
      href: "/dashboard",
      icon: HomeIcon,
      current: currentNavItem === "Inicio",
    },
    {
      name: "Pacientes",
      href: "/pacientes",
      icon: UsersIcon,
      current: currentNavItem === "Pacientes",
    },
    {
      name: "Calendario",
      href: "/calendario",
      icon: FolderIcon,
      current: currentNavItem === "Calendario",
    },
    {
      name: "Mi Perfil",
      href: "/perfil",
      icon: CalendarIcon,
      current: currentNavItem === "Mi Perfil",
    },
    {
      name: "Facturacion",
      href: "/facturacion",
      icon: DocumentDuplicateIcon,
      current: currentNavItem === "Facturacion",
    },
    {
      name: "Estadisticas",
      href: "/estadisticas",
      icon: ChartPieIcon,
      current: currentNavItem === "Estadisticas",
    },
  ];
};

export const teams = [
  {
    id: 1,
    name: "Agregar Consultorio",
    href: "/consultorio",
    initial: "+",
    current: false,
  },
];

export const userNavigation = [
  { name: "Mi Perfil", href: "/perfil" },
  { name: "Cerrar Sesión", href: "/logout" },
];

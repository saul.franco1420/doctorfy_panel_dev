export const stats = [
    { name: 'Consultas Concluidas', stat: '91', previousStat: '78', change: '12%', changeType: 'increase' },
    { name: 'Pacientes', stat: '58', previousStat: '40', change: '2.02%', changeType: 'increase' },
    { name: 'Visitas a tu perfil', stat: '24,038', previousStat: '19,390', change: '4.05%', changeType: 'decrease' },
  ]
  